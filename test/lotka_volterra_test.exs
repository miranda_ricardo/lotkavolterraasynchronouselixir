defmodule LotkaVolterraTest do
  use ExUnit.Case


  setup do
    lv = %{populations: %{foxes: 5,rabbits: 10}, 
           time:        %{time_remaining: 5, delta_t: 0.001, time_elapsed: 0}}

    {:ok, lv_stash_pid} = LotkaVolterra.LotkaVolterraStash.start_link lv
    LotkaVolterra.LotkaVolterra.start_link lv_stash_pid
    {:ok, [lv_stash_pid: lv_stash_pid, lv: lv]}
  end


  test "LV stash initial values", %{lv_stash_pid: lv_stash_pid, lv: lv} do
    assert LotkaVolterra.LotkaVolterraStash.get(lv_stash_pid) ==
      %{ populations: lv.populations, time: lv.time, r_coefs: [], f_coefs: [] } 
  end


  test "LV initial values", %{lv_stash_pid: _, lv: lv} do
    {lotka_volterra, _} = LotkaVolterra.LotkaVolterra.get 
    assert lotka_volterra ==  %{ populations: lv.populations, time: lv.time, r_coefs: [], f_coefs: [] }
  end
end
