defmodule Redis.RedisOutput do
  @moduledoc """
  Copyright (c) 2015 Ricardo C. Miranda

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
  """


  use GenServer
  import Exredis.Api


  @redis_url            "127.0.0.1"
  @redis_port           "6379"


  ## Client API


  def start_link do
    :gen_server.start_link({ :local, :redis_pid }, __MODULE__, [], [])
  end


  def set(res=%{key: _, value: _}) do
    :gen_server.cast :redis_pid, {:set, res}
  end


  def get(key) do
    :gen_server.call :redis_pid, {:get, key}
  end
  

  def stop do
    :gen_server.call :redis_pid, :stop
  end


  #####
  ## Server Callbacks


  def init(_) do
    client = Exredis.start_using_connection_string("redis://#{@redis_url}:#{@redis_port}")
    { :ok, client }
  end

  
  def handle_cast({ :set, res=%{key: _, value: %{time: _, rabbits: _, foxes: _}} }, client) do
    client |> hset "#{res.key}", "time",    "#{res.value.time}"
    client |> hset "#{res.key}", "rabbits", "#{res.value.rabbits}"
    client |> hset "#{res.key}", "foxes",   "#{res.value.foxes}"
    { :noreply, client }
  end


  def handle_call({:get, key}, _from, client) do
    value = client |> get "#{key}"
    { :reply, value, client }
  end


  @doc "Handle the server stop message"
  def handle_call(:stop, _from, client) do
    close_redis client
    { :stop, :normal, :ok, client }
  end


  def terminate(_reason, client) do
    close_redis client
  end


  @doc "Catch-all to discard unknown messages."
  def handle_info(_msg, state) do
    {:noreply, state}
  end


  #####
  # Redis 


  defp close_redis(client) do
    client |> Exredis.stop
  end
end
