defmodule LotkaVolterra.Rabbits do
  @moduledoc """
  Copyright (c) 2015 Ricardo C. Miranda

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
  """


  use GenServer


  ## Client API


  @doc """
  Initial rabbits population.
  """
  def start_link(r_stash_pid) do
    :gen_server.start_link({ :local, :rab_pid }, __MODULE__, r_stash_pid, [])
  end


  @doc """
  Asynchronous computation of coeficients for next time step implicit calculation.
  It needs current rabbits and foxes populations and time step.
  """
  def calc_coefs(foxes, rabbits, delta_t) do
    :gen_server.cast :rab_pid, {:calc_coefs, {foxes, rabbits, delta_t}}
  end
  

  def stop do
    :gen_server.call :rab_pid, :stop
  end


  ## Server Callbacks


  def init(r_stash_pid) do
    rates = LotkaVolterra.RabbitsStash.get r_stash_pid
    { :ok, {rates, r_stash_pid} }
  end


  def handle_cast({:calc_coefs, {foxes, rabbits, delta_t}}, {rates, r_stash_pid})
    when is_number(foxes)
    and  is_number(rabbits)
    and  is_number(delta_t) do

    r_coefs = fill_coefs(foxes,
                         rabbits,
                         rates.birth_rate,
                         rates.death_rate,
                         delta_t)

    LotkaVolterra.LotkaVolterra.set_rabbits r_coefs

    { :noreply, {rates, r_stash_pid} }
  end


  @doc "Handle the server stop message"
  def handle_call(:stop, _from, {rates, r_stash_pid}) do
    LotkaVolterra.RabbitsStash.stop r_stash_pid
    { :stop, :normal, :ok, {rates, r_stash_pid} }
  end


  def terminate(_reason, {rates, r_stash_pid}) do
    LotkaVolterra.RabbitsStash.set r_stash_pid, rates
  end


  @doc "Catch-all to discard unknown messages."
  def handle_info(_msg, state) do
    {:noreply, state}
  end


  #####
  ## Algorithms


  @spec fill_coefs(number, number, number, number, number) :: [number]
  def fill_coefs(f, r, br, dr, dt) do
    a = dr*r*dt
    b = 1-br*dt
    [a, b, r] 
  end
end


