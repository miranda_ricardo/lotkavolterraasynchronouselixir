defmodule LotkaVolterra.LotkaVolterraStash do
  @moduledoc """
  Copyright (c) 2015 Ricardo C. Miranda

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
  """


  use GenServer


  #####
  ## Client API


  @doc """
  Saves initial state for Lotka Volterra. 
  """
  def start_link(lv = %{populations: %{foxes: _,rabbits: _},
                        time:        %{time_remaining: _, delta_t: _, time_elapsed: _}}) do
    :gen_server.start_link(__MODULE__, lv, []) 
  end


  def get(pid) do
    :gen_server.call pid, :get
  end
  

  def set(pid, lotka_volterra = %{populations: %{foxes: _,rabbits: _},
                                  time:        %{time_remaining: _, delta_t: _, time_elapsed: _}, 
                                  f_coefs: _, r_coefs: _}) do
    :gen_server.cast pid, {:set, lotka_volterra}
  end
  

  def stop(pid) do
    :gen_server.call pid, :stop
  end


  #####
  ## Server Callbacks


  def init(lv) do
    lotka_volterra = %{populations: lv.populations, time: lv.time, f_coefs: [], r_coefs: []}
    { :ok, lotka_volterra }
  end


  def handle_cast({:set, lotka_volterra}, _) do
    { :noreply, lotka_volterra }
  end


  def handle_call(:get, _from, lotka_volterra) do
    { :reply, lotka_volterra, lotka_volterra}
  end


  @doc "Handle the server stop message"
  def handle_call(:stop, _from, state) do
    { :stop, :normal, :ok, state }
  end
end
