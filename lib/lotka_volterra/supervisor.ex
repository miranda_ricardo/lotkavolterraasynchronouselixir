defmodule LotkaVolterra.Supervisor do
  @moduledoc """
  Copyright 2015 (c) Ricardo C. Miranda 

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
  """


  use Supervisor


  def start_link(rabbits \\ %{birth_rate: 1.5, death_rate: 1}, 
                 foxes   \\ %{birth_rate: 1.0, death_rate: 3}, 
                 lv      \\ %{populations: %{foxes: 5,rabbits: 10}, 
                              time:        %{time_remaining: 5, delta_t: 0.001, time_elapsed: 0}}) do
      
    result = {:ok, sup_pid} = :supervisor.start_link(__MODULE__, [])
    start_workers(sup_pid, {rabbits, foxes, lv})

    result
  end


  def start_workers(sup_pid, {rabbits, foxes, lv}) do
    IO.puts "Waking up supervisors"
    {:ok, lv_stash_pid}  = :supervisor.start_child(sup_pid, worker(LotkaVolterra.LotkaVolterraStash, [lv])) 
    {:ok, rab_stash_pid} = :supervisor.start_child(sup_pid, worker(LotkaVolterra.RabbitsStash,       [rabbits])) 
    {:ok, fox_stash_pid} = :supervisor.start_child(sup_pid, worker(LotkaVolterra.FoxesStash,         [foxes]))
    {:ok, ui_stash_pid}  = :supervisor.start_child(sup_pid, worker(LotkaVolterra.UIStash,            []))

    :supervisor.start_child(sup_pid, supervisor(LotkaVolterra.SubSupervisorLotkaVolterra, [lv_stash_pid]))
    :supervisor.start_child(sup_pid, supervisor(LotkaVolterra.SubSupervisorRabbits,       [rab_stash_pid]))
    :supervisor.start_child(sup_pid, supervisor(LotkaVolterra.SubSupervisorFoxes,         [fox_stash_pid]))
    :supervisor.start_child(sup_pid, supervisor(LotkaVolterra.SubSupervisorUI,            [ui_stash_pid]))
    :supervisor.start_child(sup_pid, supervisor(Redis.SubSupervisorRedisOutput,           []))
  end


  def init(_) do
    supervise [], strategy: :one_for_one
  end
end
