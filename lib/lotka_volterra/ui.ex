defmodule LotkaVolterra.UI do
  @moduledoc """
  Copyright (c) 2015 Ricardo C. Miranda

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
  """


  use GenServer


  #####
  ## Client API


  @doc """
  Starts the UI. 
  """
  def start_link(ui_stash_pid) do
    :gen_server.start_link({ :local, :ui_pid }, __MODULE__, ui_stash_pid, []) 
  end


  @doc """
  Options to run the model.
  """
  def run_model(run_type) do
    :gen_server.cast :ui_pid, {:run_model, run_type}
  end


  @doc """
  Terminates model.
  """
  def end_model do
    :gen_server.cast :ui_pid, :end_model
  end


  @doc """
  Model IO.
  """
  def output(res = %{rabbits: _, foxes: _, time: _}) do
    :gen_server.cast :ui_pid, {:output, res}
  end
  

  def stop do
    :gen_server.call :ui_pid, :stop
  end


  #####
  ## Server Callbacks


  def init(ui_stash_pid) do
    ui = LotkaVolterra.UIStash.get ui_stash_pid
    { :ok, {ui, ui_stash_pid} }
  end


  def handle_cast({:output, res}, {ui, ui_stash_pid}) do
    store_res res, ui.acc
    { :noreply, {put_in(ui.acc, ui.acc + 1), ui_stash_pid} }
  end


  def handle_cast({:run_model, run_type}, {ui, ui_stash_pid}) do
    case run_type do
      :once -> LotkaVolterra.LotkaVolterra.run_model run_type 
               { :noreply, {ui, ui_stash_pid} }
      _     -> IO.puts "Invalid option to run model - module LotkaVolterra.UI."
    end
  end


  def handle_cast(:end_model, {ui, ui_stash_pid}) do
    IO.puts "End of simulation"
    { :noreply, {ui, ui_stash_pid} }
  end


  @doc "Handle the server stop message"
  def handle_call(:stop, _from, {ui, ui_stash_pid}) do
    LotkaVolterra.UIStash.stop ui_stash_pid
    { :stop, :normal, :ok, {ui, ui_stash_pid} }
  end


  def terminate(_reason, {ui, ui_stash_pid}) do
    LotkaVolterra.UIStash.set ui_stash_pid, ui
  end


  @doc "Catch-all to discard unknown messages."
  def handle_info(_msg, state) do
    {:noreply, state}
  end


  #####
  # UI


  defp store_res(res, acc) do
    IO.puts "#{res.time}, #{res.rabbits}, #{res.foxes}"
    Redis.RedisOutput.set %{key: "ilv:#{acc}", value: res}
  end
end
