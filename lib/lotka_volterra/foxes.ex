defmodule LotkaVolterra.Foxes do
  @moduledoc """
  Copyright (c) 2015 Ricardo C. Miranda

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
  """


  use GenServer


  ## Client API


  @doc """
  Foxes death and birth rates.
  """
  def start_link(f_stash_pid) do
    :gen_server.start_link({ :local, :fox_pid }, __MODULE__, f_stash_pid, [])
  end


  @doc """
  Asynchronous computation of next time step foxes population.
  """
  def calc_coefs(foxes, rabbits, delta_t) do
    :gen_server.cast :fox_pid, {:calc_coefs, {foxes, rabbits, delta_t}}
  end
  

  def stop do
    :gen_server.call :fox_pid, :stop
  end


  ## Server Callbacks


  def init(f_stash_pid) do
    rates = LotkaVolterra.FoxesStash.get f_stash_pid
    { :ok, {rates, f_stash_pid} }
  end


  def handle_cast({:calc_coefs, {foxes, rabbits, delta_t}}, {rates, f_stash_pid})
    when is_number(foxes)
    and  is_number(rabbits)
    and  is_number(delta_t) do

    f_coefs = fill_coefs(foxes,
                         rabbits,
                         rates.birth_rate,
                         rates.death_rate,
                         delta_t)

    LotkaVolterra.LotkaVolterra.set_foxes f_coefs

    { :noreply, {rates, f_stash_pid} }
  end


  @doc "Handle the server stop message"
  def handle_call(:stop, _from, {rates, f_stash_pid}) do
    LotkaVolterra.FoxesStash.stop f_stash_pid
    { :stop, :normal, :ok, {rates, f_stash_pid} }
  end


  def terminate(_reason, {rates, f_stash_pid}) do
    LotkaVolterra.FoxesStash.set f_stash_pid, rates
  end


  #####
  ## Algorithms


  @spec fill_coefs(number, number, number, number, number) :: [number]
  def fill_coefs(f, r, br, dr, dt) do
    a = 1+dr*dt
    b = br*dt*f
    [a, -b, f] 
  end
end


