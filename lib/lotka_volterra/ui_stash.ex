defmodule LotkaVolterra.UIStash do
  @moduledoc """
  Copyright (c) 2015 Ricardo C. Miranda

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
  """


  use GenServer


  #####
  ## Client API


  @doc """
  Saves initial state for the UI. 
  """
  def start_link do
    :gen_server.start_link(__MODULE__, [], []) 
  end


  def get(pid) do
    :gen_server.call pid, :get
  end
  

  def set(pid, ui = %{acc: _}) do
    :gen_server.cast pid, {:set, ui}
  end
  

  def stop(pid) do
    :gen_server.call pid, :stop
  end


  #####
  ## Server Callbacks


  def init(_) do
    { :ok, %{acc: 0} }
  end


  def handle_cast({:set, foxes}, _) do
    { :noreply, foxes }
  end


  def handle_call(:get, _from, ui) do
    { :reply, ui, ui }
  end


  @doc "Handle the server stop message"
  def handle_call(:stop, _from, state) do
    { :stop, :normal, :ok, state }
  end
end
