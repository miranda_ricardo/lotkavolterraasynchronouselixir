defmodule LotkaVolterra.LotkaVolterra do
  @moduledoc """
  Copyright (c) 2015 Ricardo C. Miranda

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
  """


  use GenServer

 
  #####
  ## Client API


  @doc """
  Starts the application, serves as engine to solve system of equations. 
  """
  def start_link(lv_stash_pid) do
    :gen_server.start_link({ :local, :lv_pid }, __MODULE__, lv_stash_pid, []) 
  end


  @doc """
  This function exsts for testing.
  """
  def get do
    :gen_server.call :lv_pid, :get
  end


  @doc"""
  List where;
   - 1st element is coeficient for foxes in foxes equation;
   - 2nd element is coeficient for rabbits in foxes equation; and
   - 3rd element is idependent term in foxes equation.
  """
  def set_foxes(f_coefs) do
    :gen_server.cast :lv_pid, {:set_foxes, f_coefs}
  end


  @doc"""
  List where;
   - 1st element is coeficient for foxes in rabbits equation;
   - 2nd element is coeficient for rabbits in rabbits equation; and
   - 3rd element is idependent term in rabbits equation.
  """
  def set_rabbits(r_coefs) do
    :gen_server.cast :lv_pid, {:set_rabbits, r_coefs}
  end


  @doc"""
  main method.
  """
  def run_model(run_type) do
    case run_type do
      :once -> :gen_server.cast :lv_pid, :run_model
      _     -> IO.puts "Invalid option to run model - module LotkaVolterra.LotkaVolterra."
    end
  end
  

  def stop do
    :gen_server.call :lv_pid, :stop
  end


  #####
  ## Server Callbacks


  def init(lv_stash_pid) do
    state = LotkaVolterra.LotkaVolterraStash.get lv_stash_pid
    { :ok, {state, lv_stash_pid} }
  end


  def handle_cast({:set_foxes, f_coefs}, {state, lv_stash_pid}) do
    case state.r_coefs do
      [] -> 
        populations = state.populations
        time = state.time 
        f_c = f_coefs
        r_c = state.r_coefs

      _  -> 
        {time_elapsed, time_remaining, foxes, rabbits} = calc_step f_coefs, state.r_coefs, 
                                                                   state.time.delta_t, state.time.time_remaining, state.time.time_elapsed
        populations = %{foxes: foxes, rabbits: rabbits}
        time = %{delta_t: state.time.delta_t, time_elapsed: time_elapsed, time_remaining: time_remaining}
        f_c = []
        r_c = []
        next_step foxes, rabbits, state.time.delta_t, state.time.time_elapsed, state.time.time_remaining
    end

    { :noreply, { %{populations: populations, time: time, f_coefs: f_c, r_coefs: r_c}, lv_stash_pid } }
  end


  def handle_cast({:set_rabbits, r_coefs}, {state, lv_stash_pid}) do
    case state.f_coefs do
      [] ->
        populations = state.populations
        time = state.time 
        f_c = state.f_coefs
        r_c = r_coefs

      _  -> 
        {time_elapsed, time_remaining, foxes, rabbits} = calc_step state.f_coefs, r_coefs, 
                                                                   state.time.delta_t, state.time.time_remaining, state.time.time_elapsed
        populations = %{foxes: foxes, rabbits: rabbits}
        time = %{delta_t: state.time.delta_t, time_elapsed: time_elapsed, time_remaining: time_remaining}
        f_c = [] 
        r_c = []
        next_step foxes, rabbits, state.time.delta_t, state.time.time_elapsed, state.time.time_remaining
    end

    { :noreply, { %{populations: populations, time: time, f_coefs: f_c, r_coefs: r_c}, lv_stash_pid } }
  end


  def handle_cast(:run_model, {state, lv_stash_pid}) do
    next_step state.populations.foxes, state.populations.rabbits, state.time.delta_t, state.time.time_elapsed, state.time.time_remaining
    { :noreply, {state, lv_stash_pid} }
  end


  @doc "Handle the server stop message"
  def handle_call(:stop, _from, {state, lv_stash_pid}) do
    LotkaVolterra.LotkaVolterraStash.stop lv_stash_pid
    { :stop, :normal, :ok, {state, lv_stash_pid} }
  end


  def handle_call(:get, _from, {state, lv_stash_pid}) do
    { :reply, {state, lv_stash_pid}, {state, lv_stash_pid}}
  end


  def terminate(_reason, {state, lv_stash_pid}) do
    LotkaVolterra.LotkaVolterraStash.set lv_stash_pid, state
  end


  @doc "Catch-all to discard unknown messages."
  def handle_info(_msg, state) do
    {:noreply, state}
  end


  #####
  ## Algorithm


  defp next_step(foxes, rabbits, delta_t, time_elapsed, time_remaining) do
    case time_remaining do
      tr when tr > 0 ->
        LotkaVolterra.UI.output %{rabbits: rabbits, foxes: foxes, time: time_elapsed}

        LotkaVolterra.Foxes.calc_coefs   foxes, rabbits, delta_t
        LotkaVolterra.Rabbits.calc_coefs foxes, rabbits, delta_t

      _              -> 
        LotkaVolterra.UI.end_model
    end
  end 


  @spec calc_step([number], [number], number, number, number) :: {number, number, number, number}
  defp calc_step(f_coefs, r_coefs, dt, tr, te) do  
    {f, r} = {(for x <- f_coefs, do: x / hd(f_coefs)),
              (for x <- r_coefs, do: x / hd(r_coefs))} |> rabbits |> foxes
    {te + dt, tr - dt, f, r}
  end


  @spec rabbits({[number], [number]}) :: {[number], number}
  defp rabbits({f_coefs, r_coefs}) do
    [_, rr, tir] = r_coefs
    [_, rf, tif] = f_coefs

    r = (tir - tif) / (rr - rf)
    {f_coefs, r}
  end


  @spec foxes({[number], number}) :: {number, number}
  defp foxes({f_coefs, r}) do
    [_, rf, tif] = f_coefs

    f = tif - rf * r
    {f, r}
  end 
end
